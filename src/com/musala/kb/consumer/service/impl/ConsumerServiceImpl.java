package com.musala.kb.consumer.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;
import org.osgi.util.tracker.ServiceTracker;

import com.musala.kb.consumer.service.ConsumerService;
import com.musala.kb.consumer.tracker.ConsoleSpammerServiceTrackerCustomizer;
import com.musala.kb.provider.service.ConsoleSpammer;

@Component (
    immediate = true
)
public class ConsumerServiceImpl implements ConsumerService {
	
	public static final String LOG_ID = "com.musala.kb.consumer.service.ConsumerServiceImpl";
	
	private ComponentContext componentContext;
	
	private ConsoleSpammer consoleSpammer;
	
	private List<ConsoleSpammer> consoleSpammersList;
	
	private ConsoleSpammer consoleSpammerFromContext;
	
	private ServiceTracker<ConsoleSpammer, ConsoleSpammer> consoleSpammerServiceTracker;	
	
//	Services and references
	
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, policyOption = ReferencePolicyOption.RELUCTANT)
	public void bindConsoleSpammer(ConsoleSpammer consoleSpammer) {
		if	(consoleSpammersList == null) {
			consoleSpammersList = new ArrayList<>();
		}
		
		consoleSpammersList.add(consoleSpammer);
	}
	
	public void unbindConsoleSpammer(ConsoleSpammer consoleSpammer) {
		this.consoleSpammersList.remove(consoleSpammer);
	}

//	lifecycle

	@Activate
	private void activate(ComponentContext context) {
		System.out.println(String.format("[%s] - Bundle has starting!", LOG_ID));
		
		this.componentContext = context;		
		
//		useConsoleSpammerServiceDirectlyFromBundleContext();
		
//		setDefaultPropertiesAndLaunchSpam();
		
		ConsoleSpammerServiceTrackerCustomizer customizer = new ConsoleSpammerServiceTrackerCustomizer(context.getBundleContext());
		consoleSpammerServiceTracker = new ServiceTracker<>(context.getBundleContext(), ConsoleSpammer.class, customizer);
		
		consoleSpammerServiceTracker.open();
		
//		System.out.println("Cantidad de servicios: " + consoleSpammerServiceTracker.getServices().length);
		
		
		System.out.println(String.format("[%s] - Bundle has started!", LOG_ID));
	}

	@Deactivate
	private void deactivate(ComponentContext context) {
		System.out.println(String.format("[%s] - Bundle has stopped!", LOG_ID));
	}
	
//	Service methods implementation
	
	@Override
	public void setDefaultPropertiesAndLaunchSpam() {
		// Service Tracker
		
		consoleSpammerServiceTracker = new ServiceTracker<ConsoleSpammer, ConsoleSpammer>
			(this.componentContext.getBundleContext(), ConsoleSpammer.class, null) {
				@Override
				public ConsoleSpammer addingService(ServiceReference<ConsoleSpammer> reference) {
					return super.addingService(reference);
				}
	
				@Override
				public void removedService(ServiceReference<ConsoleSpammer> reference, ConsoleSpammer service) {
					super.removedService(reference, service);
				}
		};
		
		consoleSpammerServiceTracker.open();
		
		// business logic
		
//		ConsoleSpammer consoleSpammerService = consoleSpammerServiceTracker.getService();
		Object[] services = consoleSpammerServiceTracker.getServices();
		System.out.println();
		
	}
	
//	Others
	
	private void useConsoleSpammerServiceDirectlyFromBundleContext() {
		System.out.println(String.format("[%s] - Using service directly from bundle context - Start!", LOG_ID));
		
		BundleContext bundleContext = this.componentContext.getBundleContext();
		
		ServiceReference<ConsoleSpammer> serviceReference = bundleContext.getServiceReference(ConsoleSpammer.class);
		consoleSpammerFromContext = bundleContext.getService(serviceReference);
		
		consoleSpammerFromContext.startSpammingConsole();
		
		System.out.println(String.format("[%s] - Using service directly from bundle context - End!", LOG_ID));
	}

}
