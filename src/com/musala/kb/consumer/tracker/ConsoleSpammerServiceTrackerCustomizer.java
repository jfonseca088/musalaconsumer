package com.musala.kb.consumer.tracker;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import com.musala.kb.provider.service.ConsoleSpammer;

public class ConsoleSpammerServiceTrackerCustomizer implements ServiceTrackerCustomizer<ConsoleSpammer, ConsoleSpammer> {

	private final BundleContext context;	
	
	public ConsoleSpammerServiceTrackerCustomizer(BundleContext context) {
		this.context = context;
	}	
	
//	Overriding

	@Override
	public ConsoleSpammer addingService(ServiceReference<ConsoleSpammer> reference) {
		ConsoleSpammer service = context.getService(reference);
		
		boolean isAlternative = reference.getProperty("type").toString().equals("c_alternative");
		
		executeSomeBusinessLogicAccordingServiceImpl(service, isAlternative);
		
		return service;
	}

	@Override
	public void modifiedService(ServiceReference<ConsoleSpammer> reference, ConsoleSpammer service) {
		System.out.println();
	}

	@Override
	public void removedService(ServiceReference<ConsoleSpammer> reference, ConsoleSpammer service) {
		context.ungetService(reference);		
	}	
	
//	Business Logic
	
	private void executeSomeBusinessLogicAccordingServiceImpl(ConsoleSpammer consoleSpammer, boolean alternative){
		Map<String, String> properties = new HashMap<String, String>();
		
		properties.put("Date", new Date().toString());
		properties.put("Title", "My Title");
		properties.put("Summary", "Custom Summary");
		
		if	(alternative) {
			properties.put("Type", "Alterntive Mode");
		}
		
		consoleSpammer.setProperties(properties);
		consoleSpammer.startSpammingConsole();
	}

}
