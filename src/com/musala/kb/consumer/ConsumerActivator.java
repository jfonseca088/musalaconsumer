package com.musala.kb.consumer;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class ConsumerActivator implements BundleActivator {

	public static final String LOG_ID = "com.musala.kb.consumer.ConsumerActivator";
	
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println(String.format("[%s] - Bundle has starting!", LOG_ID));
		
//		ConsoleSpammer spammer = new ConsoleSpammerImpl();
		
//		context.registerService(ConsoleSpammer.class, spammer, null);
		
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println(String.format("[%s] - Bundle has stopped!", LOG_ID));
	}

}
